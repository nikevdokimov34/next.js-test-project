export default function getNestedObjectData(obj, ...args) {
    return args.reduce((obj, layer) => obj && obj[layer], obj)
  }