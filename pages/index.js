import React from "react";
import MyWonderfulComponent from "../components/MyWonderfulComponent";

export async function getServerSideProps(context) {
  const message = "Hello from SSR";
  console.log(message);

  return {
    props: {
      id: 1,
      options: {
        params: {
          fields: {
            isDynamic: true,
          },
        },
      },
      count: 0,
      color: "red",
      data: "",
      componentText: message,
    },
  };
}

export default function Page({
  id,
  options,
  count,
  color,
  data,
  componentText,
}) {
  return (
    <MyWonderfulComponent
      id={id}
      options={options}
      count={count}
      color={color}
      data={data}
    >
      {componentText}
    </MyWonderfulComponent>
  );
}
