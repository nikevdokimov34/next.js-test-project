import React, { useState, useEffect } from "react";
import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import getNestedObjectData from "../utils/getNestedObject";

export default function MyWonderfulComponent({
  id,
  options,
  children,
  ...other
}) {
  const { count, color } = other;
  const [summ, setSumm] = useState(count);
  const isDynamic = getNestedObjectData(
    options,
    "params",
    "fields",
    "isDynamic"
  );

  const useStyles = makeStyles({
    heading: {
      color: color,
    },
  });

  useEffect(() => {
    id && isDynamic ? setSumm(summ + 1) : null;
  }, []);

  const classes = useStyles();

  console.log("summ", summ);

  return (
    <>
      <h1 className={classes.heading}>Hello World!</h1>
      <Grid>
        <Grid xs={12}>{children}</Grid>
      </Grid>
    </>
  );
}
